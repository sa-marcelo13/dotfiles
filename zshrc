# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
if [[ $(uname) == "Darwin" ]]; then
    alias create_java_project='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/linux/utils/./create_java_project.sh'
    alias courses='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/studies/courses'
    alias icloud='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs'
    alias studies='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/studies'
    alias books='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/studies/books'
    alias tips='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/linux/tips'
    alias utils='${HOME}/Library/Mobile\ Documents/com~apple~CloudDocs/Documents/linux/utils'
    alias xdg-open=open
else
    if [[ $(uname -r) == *"microsoft"* ]]; then
        alias create_java_project='${HOME}/Documents/utils/./create_java_project.sh'
        alias courses='${HOME}/Documents/studies/courses/'
        alias faculdade='${HOME}/Documents/studies/faculdade/'
        alias studies='${HOME}/Documents/studies/'
        alias books='${HOME}/Documents/studies/books/'
        alias tips='${HOME}/Documents/tips/'
        alias utils='${HOME}/Documents/utils/'
    else
        alias drive='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/'
        alias create_java_project='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/linux/utils/./create_java_project.sh'
        alias courses='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/studies/courses/'
        alias faculdade='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/studies/faculdade/'
        alias studies='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/studies/'
        alias books='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/studies/books/'
        alias tips='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/linux/linux_tips/'
        alias utils='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/linux/utils/'
        alias finance='${HOME}/Insync/sa98marcelo@gmail.com/Google\ Drive/finance/'
    fi
fi
alias documents='${HOME}/Documents/'
alias doc='${HOME}/Documents/'
alias dot='${HOME}/dotfiles/'
alias teste='${HOME}/Documents/teste/'
alias coded='${HOME}/Documents/code/'
alias downloads='${HOME}/Downloads/'
alias n='nvim .'
alias nc='nvim ${HOME}/.config/nvim/'
alias nz='nvim ${HOME}/dotfiles/zshrc'
alias sz='source ${HOME}/.zshrc'
alias nat='sudo virsh net-start default'
alias vim=nvim
alias ls=lsd
alias cat=bat
alias python=python3
alias pip=pip3
alias xdg-open=open
alias centos='ssh marcelo@192.168.0.112'


#java_home
export JAVA_HOME=$HOME/.asdf/installs/java/openjdk-21.0.2
export PATH=$JAVA_HOME/bin:$PATH

# add local bin in path
export PATH=/home/marcelo/.local/bin:$PATH

# add snap bin in path
export PATH=/snap/bin:$PATH


#asdf
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash

#the fuck
#eval $(thefuck --alias)

#tmux
if command -v tmux &> /dev/null && [ -z "$TMUX" ]; then
  tmux # attach-session -t 0 || tmux new-session -s 0
fi
